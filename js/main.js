import taskModel from './task/model.js'
import taskView from './task/view.js'
import taskController from './task/controller.js'

import clockModel from './clock/model.js'
import clockView from './clock/view.js'
import clockController from './clock/controller.js'

import timerModel from './pomodoro_timer/model.js'
import timerView from './pomodoro_timer/view.js'
import timerController from './pomodoro_timer/controller.js'


const mainViewTemplate = (id) => {
  return `
    <!-- Clock -->
    <div class="clock-timer clock-timer-${id}">
    </div>

    <!-- Button Control Pomodoro Timer-->
    <div class="control-btn-timer control-btn-timer-${id}">
    </div>

    <!-- Pomodoro Timer -->
    <div id="pomodoro-timer-clock-${id}">
    </div>

    <!-- Form add task to Table Body below -->
    <div class="form-add-task-${id}">
    </div>

    <!--  Table Tasks -->
    <div class="table-task table-task-${id}">
    </div>
  `
}
class Pomodoro {
  constructor(id) {
    this.app = document.querySelector('main')
    console.log(this.app);
    var element = document.createElement('div') 
    element.id = id
    this.app = this.app.appendChild(element)
    this.app.innerHTML = mainViewTemplate(id)

    this.run(id)
  }
  run(id) {
    this.showTime(id)
    this.pomodoroTimerClock(id)
    this.pomodoroTasks(id)
  }
  // Clock
  // showtime return variable time data
  showTime(id) {
    // Clalling clock
    // const clockTimer1 = new clockTimer(id);
    const clockTimer = new clockController(new clockModel(id), new clockView(id), id);
  }

  pomodoroTimerClock(id) {
    // const pomodoroTimer1 = new pomodoroTimer(id);
    const pomodoroTimer1 = new timerController(new timerModel(), new timerView(id), id);
  }

  pomodoroTasks(id) {
    // const task1 = new task(id)
    const Task = new taskController(new taskModel(id), new taskView(id), id);
  }
}

const pomodoroApp = new Pomodoro("root");




