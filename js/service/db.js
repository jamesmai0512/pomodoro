const localStorageService = {
  // Handle the task data
  save: (resource, task) => localStorage.setItem(resource, task),
  get: (resource) => localStorage.getItem(resource)
}

const restAPIService = {}

class DBService {
  constructor(env) {
    this.env = env
    this.dbService = env == 'development' ? localStorageService : restAPIService
  }

  save = (resource, item) =>  this.dbService.save(resource, item)
  
  get = (resource) => this.dbService.get(resource)
}

export default DBService