class clockModel {
  constructor(id) {
    this.id = id
  }

  addTimer() {
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();

    this.time = `${h}:${m}:${s}`;
    if ( s < 10) {
      this.time = `${h}:${m}:0${s}`;
    }
  }
}

export default clockModel