class clockView {
  constructor(id) {
    this.id = id
    var element = document.createElement('div')
    element.id = `clock-${id}`
    element.setAttribute('class', 'clock clock-display')
    this.clockDisplayTimer = document.querySelector(`.clock-timer-${id}`)
    this.clockDisplayTimer.appendChild(element)
  }

  clockTemplate(time) {
    document.getElementById(`clock-${this.id}`).innerHTML = time;
    document.getElementById(`clock-${this.id}`).textContent = time;
  }
}

export default clockView