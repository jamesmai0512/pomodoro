class clockController {
  constructor(clockModel, clockView) {
    this.clockModel = clockModel
    this.clockView = clockView

    this.clockModel.addTimer()
    const self = this 
    setInterval(() => {
      self.clockModel.addTimer()
      self.clockView.clockTemplate(self.clockModel.time)
    }, 1000);
  }
}

export default clockController