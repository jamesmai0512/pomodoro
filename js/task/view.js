// VIEW
class taskView {
  constructor(id) {
    this.id = id
  }

  // Form Add Tasks
  formAddTask() {
    const formAddTask = document.querySelector(`.form-add-task-${this.id}`);
    formAddTask.innerHTML = formAddTaskTemplate(this.id)
  }
  // Add Table Body
  tableTask() {
    const tableTask = document.querySelector(`.table-task-${this.id}`);
    tableTask.innerHTML = tableTaskTemplate(this.id)
  }
  // Add tasks at first
  addTask(tasks, id) {
    this.id = id
    const tableBodyNode = document.querySelector(`.js-task-table-body-${this.id}`);
    tableBodyNode.innerHTML = tasks.map((task) =>  addTaskTemplate(task, this.id)).join('');
  }

  // Adding new task
  addNewTask(tasks, id) {
    this.id = id
    const tableBodyNode = document.querySelector(`.js-task-table-body-${this.id}`);
    this.task = tasks[tasks.length - 1]
    tableBodyNode.innerHTML += addTaskTemplate(this.task, this.id)
  }

  // Change the task status finished
  updateTaskStatus(taskIndex, tasks) {
    this.task = tasks[taskIndex]
    const buttonRow = document.querySelector(`.cell-pom-controls-${this.id}-${this.task.id}`)
    buttonRow.innerHTML = updateTaskStatusTemplate(this.task)
  }
  // Change The task count
  updateTaskCount(taskIndex, tasks) {
    this.task = tasks[taskIndex]
    const pomCount = document.querySelector(`.cell-pom-count-${this.id}-${this.task.id}`)
    pomCount.innerHTML = `${this.task.pomodoroDone} Pomodori`
  }
}
// END VIEW
    
export default taskView
