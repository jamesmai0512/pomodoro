import DBService from '../service/db.js'
// MODEL
class taskModel {
  constructor(id) {
    this.id = id
    this.newDBService = new DBService('development')
    this.tasks = JSON.parse(this.newDBService.get(`tasks-${this.id}`)) || [] 
    // JSON.parse() to convert text into a JavaScript object:
  }

  // Methods of Task Model
  saveTaskData(tasks) {
    this.newDBService.save(`tasks-${this.id}`, JSON.stringify(tasks)) // Convert a JavaScript object into a string with JSON.stringify().
  }

  // Save Tasks
  saveTask(taskName) {
    if (taskName) {
      this.tasks.push({
        id: this.tasks.length > 0 ? this.tasks[this.tasks.length - 1].id + 1 : 1,
        taskName,
        pomodoroDone: 0,
        finished: false,
      })
      this.saveTaskData(this.tasks)
    }
  }
  // Count the Task
  taskCount(taskId) {
    const taskIndex = this.tasks.findIndex(e => e.id == taskId)
    this.tasks[taskIndex].pomodoroDone += 1
    this.saveTaskData(this.tasks)
  }
  // Delete the task following ID
  deleteTask(taskId) {
    this.tasks = this.tasks.filter(task => task.id != taskId)
    this.saveTaskData(this.tasks)
  }
  // Task user has finished
  finishedTask(taskId) {
    const taskIndex = this.tasks.findIndex(e => e.id == taskId)
    this.tasks[taskIndex].finished = true
    this.saveTaskData(this.tasks)
  }
}
// END MODEL

export default taskModel
