
// CONTROLLER
// has given @param Model and View
class taskController {
  constructor(model, view, id) {
    this.model = model
    this.view = view
    this.id = id

    this.view.formAddTask()
    this.view.tableTask()
    this.bindAddTask()
    this.handleControlButton()

    this.view.addTask(this.model.tasks, this.id)
  }
  // METHODS
  // Check Task Index
  taskIndex = (taskId) => this.model.tasks.findIndex(e => e.id == taskId) 
  // Add New Task
  onTaskChange = tasks => {
    this.view.addNewTask(tasks, this.id)
  }
  // Update task count
  onTaskCount = taskId => {
    const taskIndex = this.taskIndex(taskId)
    this.view.updateTaskCount(taskIndex, this.model.tasks)
  } 
  // Update task status
  onTaskStatusChange = taskId => {
    const taskIndex = this.taskIndex(taskId)
    this.view.updateTaskStatus(taskIndex, this.model.tasks)
  }
  // Delete task
  onDeleteTask = taskId => {
    const taskRow = document.getElementById(`row-${this.id}-${taskId}`)
    taskRow.remove()
  }
  // Binding event for buttons
  // Submit event, calling "addTask" to add data
  bindAddTask = () => {
    const pomodoroForm = document.querySelector(`.js-add-task-${this.id}`);
    pomodoroForm.addEventListener('submit', this.handleAddTask);
  }
  // Bind event to add task button
  handleAddTask = (e) => {
    e.preventDefault()
    var taskName = document.querySelector(`.js-add-task-name-${this.id}`).value;
    if (taskName) {
      this.model.saveTask(taskName)
      this.onTaskChange(this.model.tasks)
      document.querySelector(`.js-add-task-name-${this.id}`).value = '';
    }
  }
  // Bind event to control button
  handleControlButton = () => {
    const pomodoroTableBody = document.querySelector(`.js-task-table-body-${this.id}`);
    pomodoroTableBody.addEventListener("click", this.handleTaskButtonClick);
  }
  // Handling event task button inside table table
  handleTaskButtonClick = (event) => {
    const taskId = event.target.dataset.id;
    switch (true) {
      case event.target.matches('.js-status-task'):
        this.model.taskCount(taskId);
        this.onTaskCount(taskId)
        break;
      case event.target.matches('.js-task-done'):
        this.model.finishedTask(taskId)
        this.onTaskStatusChange(taskId)
        break;
      case event.target.matches('.js-delete-task'):
        this.model.deleteTask(taskId)
        this.onDeleteTask(taskId)
        break;
    }
  }
}
// END CONTROLLER

export default taskController