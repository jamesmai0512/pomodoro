class timerModel {
  constructor() {
    this.session_seconds =  "00";
    this.session_minutes = 25;
  }

  timerSession() {
    this.session_seconds = 59;
    this.session_minutes = 24;
  }
  sessionSeconds() {
    this.session_seconds = this.session_seconds - 1;
    this.session_seconds = (this.session_seconds < 10) ? "0" + this.session_seconds : this.session_seconds
  }
  sessionMinutes() {
    this.session_minutes = this.session_minutes - 1;
    this.session_minutes = (this.session_minutes < 10) ? "0" + this.session_minutes : this.session_minutes
  }
  resetSecond() {
    this.session_seconds = 60;
  }
  resetTimer() {
    this.session_minutes = 25;
    this.session_seconds =  "00";
  }
}

export default timerModel
