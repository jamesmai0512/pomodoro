class timerView {
  constructor(id) {
    this.id = id
  }

  pomodoroTimer() {
    this.timer = document.getElementById(`pomodoro-timer-clock-${this.id}`)
    var element = document.createElement('div')
    element.id = `${this.id}`
    this.timer = this.timer.appendChild(element)
    this.timer.innerHTML= `
    <div class="pomodoro-timer">
      <div class="timer">
        <div class="timer-detail">
          <h4 id="minutes-${this.id}"></h4>
          <h4>:</h4>
          <h4 id="seconds-${this.id}"></h4>
        </div>
      </div>
    </div>
    `
  }
  renderControlsButton() {
    var element = document.createElement('div')
    element.id = `btn-${this.id}`
    this.controlBtnTimer = document.querySelector(`.control-btn-timer-${this.id}`);
    this.controlBtnTimer = this.controlBtnTimer.appendChild(element)
    this.controlBtnTimer.innerHTML = `
      <button class="btn start-btn start-btn-${this.id}">start</button>
      <a href="index.html"><i class="fas fa-redo-alt"></i></a>
    `
  }
  addTimerTemplate(minutes, seconds) {
    document.getElementById(`minutes-${this.id}`).innerHTML = minutes;
    document.getElementById(`seconds-${this.id}`).innerHTML = seconds;
  }
  startTimerTemplate() {

  }
  addMinuteTimer(session_minutes) {
    document.getElementById(`minutes-${this.id}`).innerHTML = session_minutes;
  }
  addSecondTimer(session_seconds) {
    document.getElementById(`seconds-${this.id}`).innerHTML = session_seconds;
  }
}

export default timerView