var mainViewTemplate = function (id) {
    return "\n    <!-- Clock -->\n    <div class=\"clock-timer clock-timer-".concat(id, "\">\n    </div>\n\n    <!-- Button Control Pomodoro Timer-->\n    <div class=\"control-btn-timer control-btn-timer-").concat(id, "\">\n    </div>\n\n    <!-- Pomodoro Timer -->\n    <div id=\"pomodoro-timer-clock-").concat(id, "\">\n    </div>\n\n    <!-- Form add task to Table Body below -->\n    <div class=\"form-add-task-").concat(id, "\">\n    </div>\n\n    <!--  Table Tasks -->\n    <div class=\"table-task table-task-").concat(id, "\">\n    </div>\n  ");
};
var Pomodoro = /** @class */ (function () {
    function Pomodoro(id) {
        this.app = document.querySelector("main");
        this.id = id;
        var element = document.createElement("div");
        element.id = id;
        this.app = this.app.appendChild(element);
        this.app.innerHTML = mainViewTemplate(this.id);
        // this.run(this.id);
    }
    return Pomodoro;
}());
var pomodoroApp = new Pomodoro("root");
