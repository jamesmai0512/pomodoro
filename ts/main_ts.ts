const mainViewTemplate = (id: string): string => {
  return `
    <!-- Clock -->
    <div class="clock-timer clock-timer-${id}">
    </div>

    <!-- Button Control Pomodoro Timer-->
    <div class="control-btn-timer control-btn-timer-${id}">
    </div>

    <!-- Pomodoro Timer -->
    <div id="pomodoro-timer-clock-${id}">
    </div>

    <!-- Form add task to Table Body below -->
    <div class="form-add-task-${id}">
    </div>

    <!--  Table Tasks -->
    <div class="table-task table-task-${id}">
    </div>
  `;
};

class Pomodoro {
  app: any;
  id: string;
  constructor(id: string) {
    this.app = document.querySelector("main");
    this.id = id;
    var element = document.createElement("div");
    element.id = id;
    this.app = this.app.appendChild(element);
    this.app.innerHTML = mainViewTemplate(this.id);

    // this.run(this.id);
  }
  // run(this.id) {
  //   this.showTime(this.id);
  //   this.pomodoroTimerClock(this.id);
  //   this.pomodoroTasks(this.id);
  // }
  // // Clock
  // // showtime return variable time data
  // showTime(this.id) {
  //   // Calling clock
  //   // const clockTimer1 = new clockTimer(this.id);
  //   const clockTimer = new clockController(
  //     new clockModel(this.id),
  //     new clockView(this.id),
  //     this.id
  //   );
  // }

  // pomodoroTimerClock(this.id) {
  //   // const pomodoroTimer1 = new pomodoroTimer(this.id);
  //   const pomodoroTimer1 = new timerController(
  //     new timerModel(),
  //     new timerView(this.id),
  //     this.id
  //   );
  // }

  // pomodoroTasks(this.id) {
  //   // const task1 = new task(this.id)
  //   const Task = new taskController(new taskModel(this.id), new taskView(this.id), this.id);
  // }
}

const pomodoroApp = new Pomodoro("root");
